# Copyright 2015 IITP RAS
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

import sys
import json
import traceback
import logging
import signal
import urlparse

from tornado import gen
from tornado import ioloop
from tornado import httpclient
from tornado import tcpclient
from tornado import iostream
from tornado import options
from tornado.websocket import websocket_connect

import common

logger = logging.getLogger('everest_proxy.backend')

class Client(object):
    def __init__(self, proxyURI, serverAddress, serverPort, authToken):
        self.proxyURI = proxyURI
        self.serverAddress = serverAddress
        self.serverPort = serverPort
        self.token = authToken

    @gen.coroutine
    def run(self):
        self.sessionId = yield self.connect(0)
        yield self.loop()

    @gen.coroutine
    def connect(self, sessionId):
        logger.info('Connecting to proxy frontend at %s' % self.proxyURI)
        req = httpclient.HTTPRequest(
            self.proxyURI, validate_cert=False, headers = {
                'Authorization' : 'EverestProxy ' + self.token,
                'Sec-WebSocket-Protocol' : common.CONTROL_PROTOCOL_VERSION})
        try:
            self.conn = yield websocket_connect(req)
        except Exception, e:
            logger.error('Frontend connection failed: %s' % e)
            sys.exit(1)

        common.send_message(self.conn, 'SESSION', sessionId)
        msg = yield self.conn.read_message()
        if msg is None:
            logger.error('Frontend has closed the connection while waiting for session ID')
            sys.exit(1)
        msg = json.loads(msg)
        assert(msg[0] == 'SESSION-REP')
        raise gen.Return(msg[1])

    @gen.coroutine
    def loop(self):
        common.send_message(self.conn, 'RTUNNEL', 0)
        msg = yield self.conn.read_message()
        if msg is None:
            logger.error('Frontend has closed the connection while creating the tunnel')
            sys.exit(1)
        msg = json.loads(msg)
        assert(msg[0] == 'RTUNNEL-REP')
        if msg[1] == 'FAILED':
            logger.error('Error creating tunnel: %s' % msg[2])
            sys.exit(1)
        logger.info('Connection established. Tunnel endpoint: %s' % msg[2])

        if options.options.endpoint_file:
            with open(options.options.endpoint_file, 'w') as f:
                f.write(msg[2])

        while True:
            while True:
                msg = yield self.conn.read_message()
                if not msg is None:
                    break
                yield gen.sleep(0.5)
                logger.info('Frontend has closed the connection, reconnecting...')
                sessionId = yield self.connect(self.sessionId)
                if sessionId != self.sessionId:
                    logger.error('Failed to reconnect with the same session ID')
                    sys.exit(0)
                logger.info('Reconnect successful with sessionId=%d' % sessionId)
            msg = json.loads(msg)
            assert(msg[0] == 'RCONNECT')
            try:
                assert(msg[1] == 0)
                
                factory = tcpclient.TCPClient()
                local = yield factory.connect(self.serverAddress, self.serverPort)

                assert(msg[2][:1] == '/')
                dataUri = urlparse.urlparse(self.proxyURI)._replace(path=msg[2]).geturl()

                req2 = httpclient.HTTPRequest(
                    dataUri, validate_cert=False, headers = {
                        'Authorization' : 'EverestProxy ' + self.token,
                        'Sec-WebSocket-Protocol' : common.DATA_PROTOCOL_VERSION})
                tunnel = yield websocket_connect(req2)
                ioloop.IOLoop.current().add_future(
                    gen.moment, lambda f: common.ws_reader(logger, tunnel, local))
                ioloop.IOLoop.current().add_future(
                    gen.moment, lambda f: common.tcp_reader(logger, local, tunnel))
                common.send_message(self.conn, 'RCONNECT-REP', 'OK', msg[1])
            except:
                logger.exception('Failed to serve rconnect request')
                common.send_message(self.conn, 'RCONNECT-REP', 'FAILED', msg[1])

def handleException(callback):
    traceback.print_exc()
    ioloop.IOLoop.current().stop()

def signal_handler(signum, frame):
    ioloop.IOLoop.current().stop()

def main():
    signal.signal(signal.SIGINT, signal_handler)

    options.define("backend_logging_config", type=str,
                   help="path to backend logging config file",
                   callback=lambda path: logging.config.fileConfig(
                       path, disable_existing_loggers=False))
    options.define("endpoint_file", type=str,
                   help="path for endpoint information file from proxy frontend")

    argv = options.parse_command_line()
    if len(argv) != 4:
        print 'Usage: %s [--endpoint_file=<path>] <frontend proxy URI> <auth token> <server host> <server port>' % sys.argv[0]
        sys.exit(1)

    ioloop.IOLoop.current().handle_callback_exception = handleException

    client = Client(argv[0], argv[2], int(argv[3]), argv[1])

    ioloop.IOLoop.current().add_future(gen.moment, lambda f: client.run())
    ioloop.IOLoop.current().start()

if __name__ == "__main__":
    main()
