# Copyright 2015 IITP RAS
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

import os
import sys
import subprocess
import unittest
import time
import socket
import logging

from tornado import gen
from tornado import web
from tornado import testing
from tornado import httpclient

logger = logging.getLogger('everest_proxy.test')

class WebHandler(web.RequestHandler):
    @gen.coroutine
    def get(self, cat, fname):
        logger.info('Serving %s/%s' % (cat, fname))
        if cat == 'slow':
            delay = int(self.get_argument('t', '1'))
            yield gen.sleep(delay)
        if fname == 'test.html':
            self.finish('<html>Hello %s!</html>' % cat)
            return
        with open(fname, 'r') as f:
            self.finish(f.read())

class ProxyTests(testing.AsyncHTTPTestCase):
    def get_app(self):
        app = web.Application([
            (r"/([^/]+)/(.*)", WebHandler)
        ])
        return app

    def setUp(self):
        testing.AsyncHTTPTestCase.setUp(self)
        self.backends = []
        self.frontend = None

    def tearDown(self):
        for backend in self.backends:
            backend.terminate()
            backend.wait()
        if not self.frontend is None:
            self.frontend.terminate()
            self.frontend.wait()
        try:
            os.remove('addr.txt')
        except OSError:
            pass
        testing.AsyncHTTPTestCase.tearDown(self)

    def startFrontend(self, opts=[]):
        self.frontend = subprocess.Popen(['python', '-m', 'everest_proxy.server',
                                          '--backend_tokens=1234'] + opts)
        time.sleep(0.5)

    def startBackend(self, *args, **kw):
        cmd = ['python', '-m', 'everest_proxy.client', 'ws://localhost:4499/ws', '1234'] + list(args)
        if kw.get('wait', False):
            try:
                subprocess.check_call(cmd)
                self.fail('backend failure expected')
            except subprocess.CalledProcessError:
                return
        else:
            self.backends.append(subprocess.Popen(cmd))
            time.sleep(0.5)
        
    @testing.gen_test
    def testBasic(self):
        self.startFrontend()
        self.startBackend('localhost', str(self.get_http_port()))
        client = httpclient.AsyncHTTPClient(self.io_loop)
        response = yield client.fetch("http://localhost:5099/fast/test.html")
        self.assertTrue("html" in response.body)

    @testing.gen_test
    def testTwoClients(self):
        self.startFrontend()
        self.startBackend('localhost', str(self.get_http_port()))
        client = httpclient.AsyncHTTPClient(self.io_loop)
        fs = client.fetch("http://localhost:5099/slow/test.html")
        yield gen.sleep(0.5)
        ff = client.fetch("http://localhost:5099/fast/test.html")
        response = yield ff
        self.assertTrue("html" in response.body)
        response = yield fs
        self.assertTrue("html" in response.body)

    @testing.gen_test
    def testTwoBackends(self):
        self.startFrontend()
        self.startBackend('localhost', str(self.get_http_port()))
        self.startBackend('dcs.isa.ru', '80')
        client = httpclient.AsyncHTTPClient(self.io_loop)
        f1 = client.fetch("http://localhost:5099/fast/test.html")
        f2 = client.fetch("http://localhost:5098")
        [response1, response2] = yield [f1, f2]
        self.assertTrue("html" in response1.body)
        self.assertTrue("html" in response2.body)

    @testing.gen_test
    def testNoFrontend(self):
        self.startBackend('dcs.isa.ru', '80')
        client = httpclient.AsyncHTTPClient(self.io_loop)
        try:
            response = yield client.fetch("http://localhost:5099")
            self.fail("Backend must fail")
        except socket.error:
            pass

    @testing.gen_test
    def testPing(self):
        self.startFrontend(['--ping_period=1'])
        self.startBackend('localhost', str(self.get_http_port()))
        client = httpclient.AsyncHTTPClient(self.io_loop)
        response = yield client.fetch("http://localhost:5099/slow/test.html?t=2")
        self.assertTrue("html" in response.body)

    @testing.gen_test
    def testPortLimit(self):
        self.startFrontend(['--client_port_range=5099'])
        self.startBackend('localhost', str(self.get_http_port()))
        self.startBackend('dcs.isa.ru', '80', wait=True)
        client = httpclient.AsyncHTTPClient(self.io_loop)
        response = yield client.fetch("http://localhost:5099/fast/test.html")
        self.assertTrue("fast" in response.body)

    @testing.gen_test
    def testPortLimitCleanup(self):
        self.startFrontend(['--client_port_range=5099', '--wait_reconnect=0'])
        self.startBackend('localhost', str(self.get_http_port()))
        self.backends[0].terminate()
        self.startBackend('localhost', str(self.get_http_port()))
        client = httpclient.AsyncHTTPClient(self.io_loop)
        response = yield client.fetch("http://localhost:5099/fast/test.html")
        self.assertTrue("fast" in response.body)

    @testing.gen_test
    def testPortLimitNoCleanup(self):
        self.startFrontend(['--client_port_range=5099'])
        self.startBackend('localhost', str(self.get_http_port()))
        self.backends[0].terminate()
        self.startBackend('localhost', str(self.get_http_port()), wait=True)
        client = httpclient.AsyncHTTPClient(self.io_loop)
        try:
            yield client.fetch("http://localhost:5099/fast/test.html")
            self.fail('Request should fail because there are no backend running')
        except httpclient.HTTPError:
            pass

    @testing.gen_test
    def testClientReconnect(self):
        self.startFrontend(['--dbg_disconnect_backend'])
        self.startBackend('localhost', str(self.get_http_port()))
        client = httpclient.AsyncHTTPClient(self.io_loop)
        response = yield client.fetch("http://localhost:5099/fast/test.html")
        self.assertTrue("html" in response.body)
        yield gen.sleep(2)
        response = yield client.fetch("http://localhost:5099/fast/test.html")
        self.assertTrue("html" in response.body)

def all():
    tests = [
        'everest_proxy.test.ProxyTests'
    ]
    return unittest.defaultTestLoader.loadTestsFromNames(tests)

if __name__ == "__main__":
    if sys.version_info[0] == 2 and sys.version_info[1] < 7:
        testing.main()
    else:
        testing.main(failfast = True)
