# Copyright 2015 IITP RAS
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

import os
import json
import logging
import logging.config
import signal
from collections import deque
import time

from tornado import ioloop
from tornado import gen
from tornado import web
from tornado import tcpserver
from tornado import websocket
from tornado import options
from tornado import httpserver

import common

logger = logging.getLogger('everest_proxy.frontend')

def checkAuth(request, tokens, logger):
    if not 'Authorization' in request.headers:
        raise web.HTTPError(403)
    auth = request.headers['Authorization']
    spl = auth.split()
    if len(spl) != 2 or spl[0] != 'EverestProxy':
        logger.info('Rejecting %s with bad Authorization header' % request.remote_ip)
        raise web.HTTPError(403)
    if not spl[1] in tokens:
        logger.info('Rejecting %s: token not authorized' % request.remote_ip)
        raise web.HTTPError(403)

class BackendControlHandler(websocket.WebSocketHandler):
    def initialize(self, dispatcher, tokens, pingPeriod):
        self.dispatcher = dispatcher
        self.tokens = tokens
        self.pingPeriod = pingPeriod

    def select_subprotocol(self, subprotocols):
        checkAuth(self.request, self.tokens, logger)
        if not common.CONTROL_PROTOCOL_VERSION in subprotocols:
            return None
        return common.CONTROL_PROTOCOL_VERSION

    def send_rconnect(self, localId, rconnectURI):
        common.send_message(self, 'RCONNECT', localId, rconnectURI)

    def open(self):
        logger.info('New backend connection from %s' % self.request.remote_ip)

    def send_ping(self):
        try:
            logger.debug('Sending ping to backend control connection, backend %s' % self.backendId)
            self.ping('')
            ioloop.IOLoop.current().call_later(self.pingPeriod, lambda: self.send_ping())
        except websocket.WebSocketClosedError:
            logger.debug('Ping not sent: connection closed')
        
    def on_close(self):
        logger.info('Backend control connection from %s lost' % self.request.remote_ip)
        if hasattr(self, 'backendId'):
            self.dispatcher.stale_backend_connection(self.backendId)

    def on_message(self, msg):
        if msg is None:
            self.on_close()
            return

        msg = json.loads(msg)
        if msg[0] == 'SESSION':
            assert(type(msg[1]) is int)
            backendId = msg[1]
            self.backendId = self.dispatcher.new_backend_connection(
                self, backendId)
            logger.info('Got backendId %d from %s, replying with %d' %
                        (backendId, self.request.remote_ip, self.backendId))
            if self.pingPeriod > 0:
                ioloop.IOLoop.current().call_later(self.pingPeriod, lambda: self.send_ping())
            common.send_message(self, 'SESSION-REP', self.backendId)
        elif msg[0] == 'RTUNNEL':
            assert(len(msg) == 2)
            assert(type(msg[1]) is int)
            self.tunnelId = msg[1]
            endpoint = self.dispatcher.new_tunnel(self.backendId, self.tunnelId)
            if endpoint is None:
                common.send_message(self, 'RTUNNEL-REP', 'FAILED', 'Error allocationg a port')
            else:
                common.send_message(self, 'RTUNNEL-REP', 'OK', endpoint)
        elif msg[0] == 'RCONNECT-REP':
            assert(len(msg) == 3)
            assert(type(msg[2]) is int)
            if msg[1] == 'FAILED':
                self.dispatcher.rconnect_failed((self.backendId, msg[2]))

class BackendDataHandler(websocket.WebSocketHandler):
    def initialize(self, dispatcher, tokens, pingPeriod):
        self.dispatcher = dispatcher
        self.tokens = tokens
        self.conn = None
        self.pingPeriod = pingPeriod
        self.notifyClose = True

    def select_subprotocol(self, subprotocols):
        checkAuth(self.request, self.tokens, logger)
        if not common.DATA_PROTOCOL_VERSION in subprotocols:
            return None
        return common.DATA_PROTOCOL_VERSION

    def open(self, portArg):
        self.port = int(portArg)
        logger.info('New backend data connection from %s for port %d' % 
                    (self.request.remote_ip, self.port))
        self.conn, self.clientId = self.dispatcher.new_backend_data_connection(
            self, self.port)
        if self.pingPeriod > 0:
            ioloop.IOLoop.current().call_later(self.pingPeriod, lambda: self.send_ping())
        ioloop.IOLoop.current().add_future(
            gen.moment, lambda f: common.tcp_reader(logger, self.conn, self))

    def on_close(self):
        if self.conn:
            self.conn.close()
            self.conn = None
            if self.notifyClose:
                self.dispatcher.stale_backend_data_connection(self.port, self.clientId)

    def send_ping(self):
        try:
            logger.debug('Sending ping to backend data connection, client %s' % self.clientId)
            self.ping('')
            ioloop.IOLoop.current().call_later(self.pingPeriod, lambda: self.send_ping())
        except websocket.WebSocketClosedError:
            logger.debug('Ping not sent: connection closed')

    @gen.coroutine
    def on_message(self, msg):
        if msg is None:
            self.on_close()
            return
        try:
            yield self.conn.write(msg)
        except:
            logger.exception('Write to client connection failed')
            self.close()

class FrontendHandler(tcpserver.TCPServer):
    def __init__(self, dispatcher, port, tunnelId):
        tcpserver.TCPServer.__init__(self)
        self.tunnelId = tunnelId
        self.dispatcher = dispatcher
        self.listen(port)

    def handle_stream(self, stream, address):
        logger.info('New frontend connection from %s' % str(address))
        try:
            self.dispatcher.new_client_connection(self.tunnelId, address, stream)
        except:
            logger.exception('Failed to start connection forwarding')
            stream.close()

class Dispatcher(object):
    def __init__(self, portRange, endpointPattern, waitReconnect):
        self.backends = {}
        self.nextBackendId = 1
        self.portRange = portRange
        self.ports = {}
        self.endpointPattern = endpointPattern
        self.waitBackendReconnect = waitReconnect

    def new_backend_connection(self, conn, backendId):
        if backendId != 0 and backendId in self.backends:
            if self.backends[backendId]['connection'] is None:
                self.backends[backendId]['connection'] = conn
                return backendId
            logger.warn('Reconnect attempt from backend %d when there is a connection' %
                        backendId)
        backendId = self.nextBackendId
        self.backends[backendId] = {
            'connection' : conn,
            'tunnels' : {}
        }
        self.nextBackendId += 1
        return backendId

    def stale_backend_connection(self, backendId):
        self.backends[backendId]['connection'] = None
        self.backends[backendId]['disconnect-time'] = time.time()
        
    def free_stale_backend_connections(self):
        delete = []
        for backendId in self.backends:
            if self.backends[backendId]['connection'] is None:
                dif = time.time() - self.backends[backendId]['disconnect-time']
                if dif >= self.waitBackendReconnect:
                    for localId, tun in self.backends[backendId]['tunnels'].iteritems():
                        tun['handler'].stop()
                        self.free_port(tun['port'])
                        for clientId, client in tun['clients'].iteritems():
                            client['stream'].close()
                            client['backend-conn'].notifyClose = False
                            client['backend-conn'].close()
                    delete.append(backendId)
        for backendId in delete:
            del self.backends[backendId]

    def acquire_port(self, tunnelId):
        port = self.portRange.pop()
        assert(not port in self.ports)
        self.ports[port] = tunnelId
        return port

    def free_port(self, port):
        self.portRange.append(port)
        del self.ports[port]

    def new_tunnel(self, backendId, localId):
        try:
            port = self.acquire_port((backendId, localId))
        except IndexError:
            logger.info('No free port for tunnel %d from backend %d, trying to free some ports' %
                         (localId, backendId))
            self.free_stale_backend_connections()
            try:
                port = self.acquire_port((backendId, localId))
            except IndexError:
                logger.info('Still no free port for tunnel %d from backend %d' %
                            (localId, backendId))
                return None

        try:
            handler = FrontendHandler(self, port, (backendId, localId))
            self.backends[backendId]['tunnels'][localId] = {
                'handler' : handler,
                'port' : port,
                'clients' : {},
                'nextClientId' : 0
            }
            return self.endpointPattern % port
        except:
            self.free_port(port)
            raise

    def new_client_connection(self, (backendId, localId), address, stream):
        tunnelInfo = self.backends[backendId]['tunnels'][localId]
        if self.backends[backendId]['connection'] is None:
            logger.warn('Client connect attempt while waiting for backend %d reconnect' % 
                        backendId)
            return
        tunnelInfo['clients'][tunnelInfo['nextClientId']] = {
            'address' : address,
            'stream' : stream,
            'backend-conn' : None
        }
        self.backends[backendId]['connection'].send_rconnect(
            localId, "/ws/t%d" % (tunnelInfo['port']))
        tunnelInfo['nextClientId'] += 1

    def new_backend_data_connection(self, backendConn, port):
        backendId, localId = self.ports[port]
        tunnelInfo = self.backends[backendId]['tunnels'][localId]
        for clientId, client in tunnelInfo['clients'].iteritems():
            if client['backend-conn'] is None:
                client['backend-conn'] = backendConn
                return client['stream'], clientId
        assert(False)

    def rconnect_failed(self, (backendId, localId)):
        tunnelInfo = self.backends[backendId]['tunnels'][localId]
        for clientId, client in tunnelInfo['clients'].iteritems():
            if client['backend-conn'] is None:
                client['stream'].close()
                del tunnelInfo['clients'][clientId]
                break

    def stale_backend_data_connection(self, port, clientId):
        logger.info('Backend data connection lost for port %d' % port)
        backendId, localId = self.ports[port]
        tunnelInfo = self.backends[backendId]['tunnels'][localId]
        del tunnelInfo['clients'][clientId]

        if options.options.dbg_disconnect_backend and backendId == 0:
            logger.info('Disconnecting backend 0 for testing purposes')
            self.backends[backendId]['connection'].close()

def signal_handler(signum, frame):
    ioloop.IOLoop.current().stop()

def main():
    signal.signal(signal.SIGINT, signal_handler)

    options.define("backend_port", default=4499, type=int,
                   help="TCP port for WebSocket connections from backends")
    options.define("backend_tokens", default=[], type=str, multiple=True,
                   help="Tokens for backend authorization")
    options.define("client_endpoint_pattern", default="localhost:%d",
                   help="pattern used to generate endpoints for forwarded connections. \
                   Local port number is substituted instead of %%d")
    options.define("client_port_range", default=range(5000, 5100), type=int, multiple=True,
                   help="TCP port range for client connections")
    options.define("ssl_cert", default=None,
                   help="SSL certificate for backend connections")
    options.define("ssl_key", default=None,
                   help="SSL certificate private key for backend connections")
    options.define("frontend_logging_config", type=str,
                   help="path to frontend logging config file",
                   callback=lambda path: logging.config.fileConfig(path, disable_existing_loggers=False))
    options.define("config", type=str, help="path to config file",
                   callback=lambda path: options.parse_config_file(path, False))
    options.define("ping_period", default=10, type=int,
                   help="ping period in seconds for WebSocket connections (0 for no pings)")
    options.define("wait_reconnect", default=60, type=int,
                   help="backend reconnect timeout. Port can be freed after this time")
    options.define("dbg_disconnect_backend", default=False, type=bool,
                   help="for testing purposes, disconnect backend 0 after every client connection")

    options.parse_command_line()

    dispatcher = Dispatcher(options.options.client_port_range,
                            options.options.client_endpoint_pattern,
                            options.options.wait_reconnect)

    ssl_options = None
    if options.options.ssl_cert and options.options.ssl_key:
        ssl_options = {
            "certfile" : options.options.ssl_cert,
            "keyfile" : options.options.ssl_key,
        }
        logger.info('SSL configured')

    app = web.Application([
        (r"/ws", BackendControlHandler, {
            'dispatcher' : dispatcher,
            'tokens' : options.options.backend_tokens,
            'pingPeriod' : options.options.ping_period}),
        (r"/ws/t([0-9]+)", BackendDataHandler, {
            'dispatcher' : dispatcher,
            'tokens' : options.options.backend_tokens,
            'pingPeriod' : options.options.ping_period}),
    ], debug=True)

    server = httpserver.HTTPServer(app, ssl_options=ssl_options)
    server.listen(options.options.backend_port)

    ioloop.IOLoop.current().start()
    logging.shutdown()

if __name__ == "__main__":
    main()
