# Copyright 2015 IITP RAS
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

import json

from tornado import gen
from tornado import iostream

CONTROL_PROTOCOL_VERSION = 'v1.proxy_control.everest'
DATA_PROTOCOL_VERSION = 'v1.proxy_data.everest'

def send_message(conn, msgType, *args):
    conn.write_message(json.dumps([msgType] + list(args)))

@gen.coroutine
def ws_reader(logger, rconn, wconn):
    while True:
        data = yield rconn.read_message()
        if data is None:
            wconn.close()
            return
        try:
            yield wconn.write(data)
        except:
            logger.exception('Write to TCP connection failed')
            rconn.close()
            return

@gen.coroutine
def tcp_reader(logger, rconn, wconn):
    while True:
        try:
            data = yield rconn.read_bytes(2 ** 20, partial=True)
        except iostream.StreamClosedError:
            wconn.close()
            return
        try:
            yield wconn.write_message(data, binary=True)
        except:
            logger.exception('Write to WS connection failed')
            rconn.close()
            return
