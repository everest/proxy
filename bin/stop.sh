#!/bin/bash
BIN_DIR=`dirname "$(cd ${0%/*} && echo $PWD/${0##*/})"`
PROXY_HOME=`dirname "$BIN_DIR"`
cd $PROXY_HOME

PID_FILE=proxy.pid

running() {
    ps -p $(cat proxy.pid) -o pid | grep $(cat proxy.pid) > /dev/null
}

if [ -f $PID_FILE ] && running; then
    PID=$(cat $PID_FILE)
    kill -SIGINT $PID
    while running; do
        sleep 1
    done
    rm $PID_FILE
    echo "Proxy is stopped"
else
    echo "Proxy is not running"
fi
