#!/bin/bash
BIN_DIR=`dirname "$(cd ${0%/*} && echo $PWD/${0##*/})"`
PROXY_HOME=`dirname "$BIN_DIR"`
cd $PROXY_HOME

PID_FILE=proxy.pid

if [ -f $PID_FILE ] && [ -f /proc/$(cat $PID_FILE)/status ]; then
   echo "Proxy is already running"
else
    # Find latest Python 2.x version
    PYTHON=$(ls /usr/bin/python2.? | tail -n1)
    # Make sure Python is 2.6 or later
    PYTHON_OK=`$PYTHON -c 'import sys; print (sys.version_info >= (2, 6) and "1" or "0")'`
    if [ "$PYTHON_OK" = '0' ]; then
        echo "Python versions < 2.6 are not supported"
        exit 1
    fi
    PYTHONPATH=$(dirname $0)/..:$PYTHONPATH $PYTHON -m everest_proxy.server --config=$PROXY_HOME/conf/proxy.conf 2>errors.txt $@ &
    echo $! > $PID_FILE
    echo "Proxy is started"
fi
